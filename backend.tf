terraform {
  backend "s3" {
    bucket = "proevolua-aula-terraform"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}