resource "aws_key_pair" "proevolua" {
  key_name   = "proevolua-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDtinJLiEx4EL32IVQEieSFBX6TLjPSNo1fxZhvQ6M5N5WYMPSBfHC7H2oGReDNYazLaPwUpCNeyNQtrPYOUHXIP//XFMzLYVxaMSmyC9Ug+4Xqr25FiJK5Iu5A3FG6NzuXsNZCdwhFshi5ixkbRHG141Bi7+zlXfsiytTugjaeiwoXQSWI6lDVgSdlHm7CI6DAE2tvFxoH5oj5xYRM4EK6kKOu5Vf1VLt/YUpDN9WyuG91jK+/PqGI7Dbd9YFCl4emZkX/m0Rt8jlSbWIDJmhuQ0LgN3/0WMvssYBuW5icfpYpNlvUXHLOMlNKEuWGXHYi4N7JE+NQCAcMUHIGyp1q96lhqzpCuOihEiCF99/YgH9U04nAKX97v15rKXMgxlkAoEMsCO4ON9mcCoImu7IPBetLAnvMpopANkZiExXxHW+JCvUSHsBYuqW1PkFe4rtg+y46gnBRksWlkgJijSt0kKpgMdvWJrS6HLbAFL6j+9q4fvdmKslMKTSfUPBk9WE= ubuntu"
}

resource "aws_instance" "proevolua-1" {
  ami                    = "ami-083654bd07b5da81d"
  instance_type          = "t2.micro"
  key_name               = "proevolua-key"
#  vpc_security_group_ids = ["sg-0c7e6fe4c20cb84f8"]

  tags = {
    Name = "server1"
  }

}



resource "aws_instance" "proevolua-2" {
  ami                    = "ami-083654bd07b5da81d"
  instance_type          = "t2.micro"
  key_name               = "proevolua-key"
#  vpc_security_group_ids = ["sg-0c7e6fe4c20cb84f8"]

  tags = {
    Name = "server2"
  }

}


resource "aws_instance" "proevolua-3" {
  ami                    = "ami-083654bd07b5da81d"
  instance_type          = "t2.micro"
  key_name               = "proevolua-key"
#  vpc_security_group_ids = ["sg-0c7e6fe4c20cb84f8"]

  tags = {
    Name = "server3"
  }

}

